/**
 * Disponibilidad.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    schema: true,
    attributes: {
      id_doctor: {
        type: 'string',
        model: 'Doctor',
        via: 'id'
      },
      id_especialidad: {
        type: 'string',
        model: 'Especialidad',
        via: 'id'
      },
      id_centro_h: {
        type: 'string',
        model: 'LocalHospital',
        via: 'id'
      },
      time_star: {
        type: 'date'
      },
      time_end: {
        type: 'date'
      },
      phone: {
        type: 'string'
      },
      email: {
        type: 'email'
      },
      description: {
        type: 'text'
      }
    }
  }
};

