/**
 * LocalHospital.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    schema: true,
    attributes: {
      rif:{
        type: 'string',
        unique: true,
        required: true
      },
      first_name: {
        type: 'string'
      },
      city: {
        type: 'string'
      },
      country: {
        type: 'string'
      },
      address: {
        type: 'text'
      }
    }
  }
};

