/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/':                        'HomeController',

  'POST /doctor':             'DoctorController.create',
  'GET /doctor':              'DoctorController.find',
  'GET /doctor/:id':          'DoctorController.findOne',
  'DEL /doctor/:id':          'DoctorController.destroy',
  'PUT /doctor/:id':          'DoctorController.update',

  'POST /disponibilidad':     'DisponibilidadController.create',
  'GET /disponibilidad':      'DisponibilidadController.find',
  'GET /disponibilidad/:id':  'DisponibilidadController.findOne',
  'DEL /disponibilidad/:id':  'DisponibilidadController.destroy',
  'PUT /disponibilidad/:id':  'DisponibilidadController.update',

  'POST /hospital':           'LocalHospitalController.create',
  'GET /hospital':            'LocalHospitalController.find',
  'GET /hospital/:id':        'LocalHospitalController.findOne',
  'DEL /hospital/:id':        'LocalHospitalController.destroy',
  'PUT /hospital/:id':        'LocalHospitalController.update',

  'POST /especialidad':       'EspecialidadController.create',
  'GET /especialidad':        'EspecialidadController.find',
  'GET /especialidad/:id':    'EspecialidadController.findOne',
  'DEL /especialidad/:id':    'EspecialidadController.destroy',
  'PUT /especialidad/:id':    'EspecialidadController.update',

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
